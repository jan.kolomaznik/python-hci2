Python - základy programování
=============================

Python je jednoduchý a zároveň mocný programovací jazyk. 
Přestože se velmi lehce se učí, využívá se v různých oblastech - od zpracování vědeckých dat, přes zpracování obrazu až k výrobě webových stránek.
Pomocí Pythonu můžete zautomatizovat pracné části své práce a tím si zjednodušit život.
V rámci školení se seznámíte s fungováním jazyka Python, podíváte se na složitější příkazy a možnostmi hledání a využití standardní knihovny. 
Součástí kurzu jsou samozřejmě také praktická cvičení, během nichž mimo jiné vyzkoušíte vytvoření automatizace dvou úkolů. 

Náplň kurzu:
------------

- **[Seznámení s jazykem Python](00-python.ipynb)**
    - Charakteristika
    - K čemu se python hodí?
        - Kdo python používá?
    - Instalace
        - Python
        - Virtuální prostředí (venv)
    - Možnosti vývoje
        - Python interaktivní konsole
        - Python script/zdrojový kód
        - IPtyhon/Jupyter
    * Instalace
    * Editory
- **[Základy jazyka](01-python.basic.ipynb)**
    - Komentáře
    - Vystup konsole (`print()`)
        - Příklady implicitního vypisování
    - Proměnné
    - Vstup z konsole (`input()`)
    - Pravidla pro pojmenování proměnných
    - Operátory přiřazení
        - Operátor přiřazení kombinovavý s operací
    - Rezervovaná slova
    - Konce řádků a středníky
    - Import funkci, objeku, modulu
    * Komentáře
    * Příkazy `print()`/`input()`
    * Proměnné
- **[Datové typy](02-python.type.ipynb)**
    - Základní datové typy
    - Prázdná hodnota
    - Logické hodnoty (bool)
        - Co je v Pythonu True?
        - Opertory porovnání
    - Číselné datové typy
        - Základní operace 
        - Porovnávání
    - Řetězce (str)
        - Skladní řetězců
    - Řetězec jako seznam
    - Konverze datových typů
    * Logická typ
    * Šíselné typy
    * Řetězce
- **[atové struktury](03-python.structure.ipynb)**
    - N-tice (tuple)
    - Seznam (list)
        - Operace se seznamy (nemění vstupní seznam)
        - Vyhledání v seznam
        - Modifikace seznamu
        - Generování seznamu čísel
    - Množina (set)
        - Testy nad množinami
        - Množinové operace (nemění vstupní množinu)
        - Modifikace množin
    - Slovníky (dict)
        - Dotazu na slovnik
        - Modifikace slovníku
    * N-tice
    * Seznamy
    * Množiny
    * Slovníky
- **[Základní konstrukce jazyka](04-python.construction.ipynb)**
        - Řízení toku programu
    - Podmínka (`if`)
    - Smyčka (`for`)
        - Funkce `range()`
        - řetězce
        - Cyklus přes seznam s indexem
        - Procházení vícerozměrných listů
    - Smyčka `while`
    - Specialitka: přerušení vnějšího cyklu
    * Struktura kódu
    * Konstrukce `if` 
    * Konstrukce `for`
    * Konstrukce `while`
    - **[Základní konstrukce jazyka](05-python.construction.comprehensions.ipynb)**
- **[Funkce](06-python.function.ipynb)**
    - Vychytávky s funkcemi
        - Výchozí hodnoty argumentů
        - Proměnný počat argumentů
        - Funkce do funkce a návratové typy
        - Dokumentace funkce
    * Deklarace funkcí
    * Nepovinné a pojmenované argumenty
    * Procedury
    * Dokumentační řetězce
    - **[Lambdas](07-python.function.lambdas.ipynb)**
- **[Třídy a objekty](08-python.object.ipynb)**
    - Základní pojmy OOP
        - Třída
        - Instance (objekt)
    - OOP v Pythonu
        - Metody a `self`
        - Konstruktor `__init__`
        - Atributy (proměnná patřící objektu)
        - Privátní atributy
    - Dědičnost
        - Komplexnější příklad
    - Polymorfismus
    - Skládání
        - Skládání nebo dědičnost?
        - Atributy třídy
        - Metody třídy
        - Destruktor
    - Magické metody
        - Metoda `__str__`
        - Metoda `__repr__`
        - Metoda `__add__`
        - Metoda `__len__`
        - Metoda `__eq__`
        - Metoda `__getattribute__`
        - Metoda `__iter__` a `__next__`
    - Třída - defaultní atributy
    - Dokumentace třídy
    - Zjišťování typu
    * Definice tříd
    * Objekty
    * Atributy
    * Dědičnost
- **[Chyby a výjimky](09-python.except.ipynb)**
    - Zachycení a ošetření výjimek
    - Kontext výjimek
    - Vyvolání výjimky
- **[Řetězce](10-python.string.ipynb)**
    - Formátování řetězce
        - Speciální znaky pro textovou konzoli, pouze pro `print()`
    * Formátování řetězce
    * Regulární výrazy
    - **[Řetězce a regulární výrazy](11-python.string.re.ipynb)**
- **[Zpracování dat](12-python.file.ipynb)**
    - Mody otevření soubotu
    * Práce se soubory a adresáři
- **[Analýza dat v Pythonu](13-pandas.ipynb)**
    - Proces analýzy dat
    - Pandas
    - **[Sloupce (Series)](14-pandas.series.ipynb)**
        - Vytvoření sloupce
        - Zjišťování dat o Serii
            - Statistické informace
        - Operace
        - Intexy, řezy a masky
            - Metoda `loc` a `iloc`
        - Spojování serii
        - Další metody
    - **[Tabulky (DataFrame)](15-pandas.dataframe.ipynb)**
        - Výběr prvků
            - Výběr podle masky
            - Přidání Series do DataFrame
        - Osy DataFrame
            - Prohození os
        - Mazání z DaraFrame
        - Přejmenování
        - Indexer `loc`
        - Indexer `iloc`
        - Indexy
        - NaN neboli NULL či N/A
        - Merge
            - Příklad:
        - Groupby
        - Pivot table
    - **[Grafy](16-pandas.matploth.ipynb)**
        - Přesýpání dat
- **[Příklad na zpracování dat](17-python.construction.exersise.ipynb)**
